from check_modbus import read_data


def print_registers(address, registers):
    i = address
    for register in registers:
        print("[{0:4d}] [{1:5d}]".format(i, register))
        i += 1


with open('devices.txt', 'rb') as f:
    lines = f.readlines()
    for line in lines:
        line = line.replace('\n', '')
        print('Device: ' + line)
        try:
            data = read_data('/dev/' + line)
            print_registers(1720, data)
        except Exception as e:
            print(e)
        print('\n')
