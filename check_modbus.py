import argparse
import time

import serial
from modbus_tk import modbus_rtu
from modbus_tk import defines as cst


def read_data(port):
    master = modbus_rtu.RtuMaster(serial.Serial(
        port, baudrate=115200, bytesize=8, parity='N', stopbits=1))

    def init():
        try:
            master.set_timeout(5)
            master.set_verbose(True)
            master.open()
            time.sleep(2)
            return True
        except Exception as ex:
            print(ex)
            return False

    def read_hregs(id, start, count):
        return master.execute(id, cst.READ_HOLDING_REGISTERS, start, count)

    init()
    result = read_hregs(3, 1720, 100)
    master.close()
    return result


def print_registers(registers):
    with open('results.txt', 'a') as f:
        f.write(" ".join([str(i) for i in registers]) + "\n")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("port", type=str)
    args = parser.parse_args()
    data = read_data(args.port)  # /dev/ttyO1
    print_registers(data)
